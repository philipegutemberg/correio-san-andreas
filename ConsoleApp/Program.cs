﻿using CorreiosSanAndreas;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CorreiosSanAndreas.Dijkstra;

namespace ConsoleApp
{
	class Program
	{
		static void Main(string[] args)
		{
			MainAsync(args).GetAwaiter().GetResult();
		}

		static async Task MainAsync(string[] args)
		{
			Console.WriteLine("Insira o caminho do arquivo de trechos:");
			string caminhoArquivoTrechos = Console.ReadLine();

			Console.WriteLine("Insira o caminho do arquivo de encomendas:");
			string caminhoArquivoEncomendas = Console.ReadLine();

			List<SolvedRoute> routesToSolve;
			using (PackagesFile packagesFile = new PackagesFile(caminhoArquivoEncomendas))
			{
				routesToSolve = (await packagesFile.GetRoutesToSolve()).ToList();

				using (RoutesFile routesFile = new RoutesFile(caminhoArquivoTrechos))
				{
					IList<Route> routes = await routesFile.GetAllRoutes();

					int[,] graph = routes.MapToGraph();

					IList<string> nodes = routes.GetAllNodes();

					routesToSolve.ForEach(routeToSolve =>
					{
						int sourceIndex = nodes.IndexOf(routeToSolve.Source);

						MinDistance[] minDistances = new Dijkstra().Solve(sourceIndex, graph);

						int targetIndex = nodes.IndexOf(routeToSolve.Target);

						routeToSolve.SaveShortPath(minDistances, nodes, sourceIndex, targetIndex);
					});
				}
			}

			if (File.Exists("rotas.txt"))
				File.Delete("rotas.txt");

			using (StreamWriter sw = new StreamWriter("rotas.txt"))
			{
				routesToSolve.ForEach(rs =>
				{
					sw.WriteLine($"{string.Join(" ", rs.ShortestPath)} {rs.Distance}");
				});
			}

			Console.WriteLine("Sua solução encontra-se no arquivo 'rotas.txt' ;)");
			Console.ReadLine();
		}
	}
}
