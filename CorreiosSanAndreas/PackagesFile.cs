﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorreiosSanAndreas
{
	public class PackagesFile : ReadableFile
	{
		public PackagesFile(string filePath)
			: base(filePath)
		{
		}

		public async Task<IList<SolvedRoute>> GetRoutesToSolve()
		{
			List<string> lines = (await this.GetAllLines()).ToList();

			return lines.ConvertAll(line =>
			{
				string[] lineParts = line.Split(' ');

				if (lineParts.Count() != 2)
					throw new Exception($"The follow line on file is not valid: {line}.");

				return new SolvedRoute(lineParts[0].Trim(), lineParts[1].Trim());
			});
		}
	}
}
