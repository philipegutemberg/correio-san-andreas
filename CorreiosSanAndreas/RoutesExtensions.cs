﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorreiosSanAndreas
{
	public static class RoutesExtensions
	{
		public static int[,] MapToGraph(this IList<Route> routes)
		{
			IList<string> nodes = routes.GetAllNodes();

			int[,] graph = new int[nodes.Count, nodes.Count];

			routes.ToList().ForEach(route =>
			{
				int sourceIndex = nodes.IndexOf(route.Source);
				int targetIndex = nodes.IndexOf(route.Target);

				graph[sourceIndex, targetIndex] = route.Distance;
			});

			return graph;
		}

		public static IList<string> GetAllNodes(this IList<Route> routes)
		{
			List<string> nodes = new List<string>();

			nodes.AddRange(routes.Select(r => r.Source));
			nodes.AddRange(routes.Select(r => r.Target));

			nodes = nodes.Distinct().OrderBy(n => n).ToList();

			return nodes;
		}
	}
}
