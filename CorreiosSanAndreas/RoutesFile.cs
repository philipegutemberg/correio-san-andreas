﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorreiosSanAndreas
{
	public class RoutesFile : ReadableFile
	{
		public RoutesFile(string filePath)
			: base(filePath)
		{
		}

		public async Task<IList<Route>> GetAllRoutes()
		{
			List<string> lines = (await this.GetAllLines()).ToList();

			return lines.ConvertAll(line =>
			{
				string[] lineParts = line.Split(' ');

				if (lineParts.Count() != 3)
					throw new Exception($"The follow line on file is not valid: {line}.");

				if (!int.TryParse(lineParts[2].Trim(), out int distance))
					throw new Exception($"Can't extract the distance path on the follow line: {line}.");

				return new Route(lineParts[0].Trim(), lineParts[1].Trim(), distance);
			});
		}
	}
}
