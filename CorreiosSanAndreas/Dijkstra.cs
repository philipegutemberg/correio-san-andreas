﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorreiosSanAndreas
{
	public class Dijkstra
	{
		public MinDistance[] Solve(int sourceNodeIndex, int[,] graph)
		{
			MinDistance[] minDistancesState = new MinDistance[graph.GetLength(0)];
			bool[] shortPathSet = new bool[graph.GetLength(0)];

			minDistancesState = minDistancesState.Select(m => new MinDistance { Distance = int.MaxValue, CommingFrom = 0 }).ToArray();

			minDistancesState[sourceNodeIndex].Distance = 0;

			for (int i = 0; i < graph.GetLength(0) - 1; i++)
			{
				int minDistanceIndex = this.GetMinValueIndex(minDistancesState, shortPathSet, out int minDistance);
				shortPathSet[minDistanceIndex] = true;

				for (int u = 0; u < graph.GetLength(0); u++)
				{
					int currentPathDistance = graph[minDistanceIndex, u];

					if (!shortPathSet[u]
						&& currentPathDistance > 0
						&& (minDistance + currentPathDistance) < minDistancesState[u].Distance)
					{
						minDistancesState[u].Distance = minDistance + currentPathDistance;
						minDistancesState[u].CommingFrom = minDistanceIndex;
					}
				}
			}

			return minDistancesState;
		}

		private int GetMinValueIndex(MinDistance[] minDistancesState, bool[] shortPathSet, out int minDistance)
		{
			Dictionary<int, int> minDistances = minDistancesState.Select((elem, idx) =>
			{
				return new
				{
					Index = idx,
					Distance = elem.Distance
				};
			}).ToDictionary(d => d.Index, d => d.Distance);

			KeyValuePair<int, int> min = minDistances.Where(m => !shortPathSet[m.Key]).OrderBy(m => m.Value).First();

			minDistance = min.Value;
			return min.Key;
		}

		public class MinDistance
		{
			public int Distance { get; set; }
			public int CommingFrom { get; set; }
		}
	}
}
