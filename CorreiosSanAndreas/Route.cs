﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorreiosSanAndreas
{
	public class Route
	{
		public string Source { get; private set; }
		public string Target { get; private set; }
		public int Distance { get; set; }

		public Route(string source, string target, int distance)
		{
			this.Source = source;
			this.Target = target;
			this.Distance = distance;
		}
	}
}
