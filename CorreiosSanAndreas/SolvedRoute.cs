﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CorreiosSanAndreas.Dijkstra;

namespace CorreiosSanAndreas
{
	public class SolvedRoute : Route
	{
		public SolvedRoute(string source, string target)
			: base(source, target, 0)
		{
			this.ShortestPath = new List<string>();
		}

		public List<string> ShortestPath { get; set; }

		public void SaveShortPath(MinDistance[] minDistances, IList<string> nodes, int sourceIndex, int targetIndex)
		{
			List<int> path = this.SaveCurrentAndGoToCommingFrom(minDistances, new List<int>(), targetIndex, sourceIndex);

			this.ShortestPath = path.Select(p => nodes[p]).Reverse().ToList();

			this.Distance = minDistances[targetIndex].Distance;
		}

		private List<int> SaveCurrentAndGoToCommingFrom(MinDistance[] minDistances, List<int> path, int currentIndex, int sourceIndex)
		{
			path.Add(currentIndex);

			if (currentIndex == sourceIndex)
				return path;

			return this.SaveCurrentAndGoToCommingFrom(minDistances, path, minDistances[currentIndex].CommingFrom, sourceIndex);
		}
	}
}
