﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorreiosSanAndreas
{
	public class ReadableFile : IDisposable
	{
		protected StreamReader _streamReader;
		protected IList<string> _fileLines;

		public ReadableFile(string filePath)
		{
			if (!File.Exists(filePath))
				throw new FileNotFoundException($"The file was not found. Path: {filePath}.");

			_streamReader = new StreamReader(filePath);
		}

		public async Task<IList<string>> GetAllLines()
		{
			if (_fileLines == null)
			{
				_fileLines = new List<string>();

				while (!_streamReader.EndOfStream)
				{
					_fileLines.Add(await _streamReader.ReadLineAsync());
				}
			}

			return _fileLines;
		}

		public void Dispose()
		{
			if (_streamReader != null)
				_streamReader.Close(); /* According to the documentation, the Close method also calls Dispose on Stream. So, we're ok here ;) */
		}
	}
}
